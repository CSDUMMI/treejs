# TreeJS

A library for working with arbitrary trees in JS

# Operations on a tree
A tree can be added to, traversed and 
removed from.

## Adding to trees
You can add to a tree by adding a new node to a parent node.

## Traversing trees
Traversal is provided a function that takes an array
of values and returns a value.

```
traverse :: Tree -> (Node -> [a] -> a) -> a
```
It takes a tree and a function that gets a node
and an array of values and converts those into
a single value.

This function is first applied to all the tips
with an empty array. Then each of their parents 
is applied to the function with the results
of the children. Until the root was reached.

## Search
Searching a tree means providing a predicate and 
going through the tree until a node is found that satisfies 
the predicate.
