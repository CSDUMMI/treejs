class TreeNode {
    constructor(data) {
	this.data = data
    	this.children = []
    } 
    
    /**
     * @param childData data of the new child to add
     */
    addChild(childData) {
	this.children.push(new TreeNode(childData))
    }
    
    /**
     * @callback traversalFn
     * @param data the data of the node
     * @param res {Array<a>} the resulting data from the child nodes
     * @returns {a}
     */

    /**
     * Traverse the tree
     * @param fn {traversalFn} function to traverse the tree with.
     */
    traverse(fn) {
	let childrenRes = this.children.map(c => c.traverse(fn))
	return fn(this.data, childrenRes)
    }
    
    /**
     * @param fn a predicate to test the data of nodes
     * @returns {TreeNode|null} the first node satisfying fn or null if none did.
     */
    search(fn) {
	if(fn(this.data) == true) {
		return this.data
	} else {
		return filterUntil(fn, this.children)
	}
    }
}

function filterUntil(fn, arr) {
	for(let x of arr) {
		if(fn(x)) {
			return x
		}
	}
	return null
}

module.exports = TreeNode
